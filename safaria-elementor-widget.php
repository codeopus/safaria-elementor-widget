<?php
/**
 * Plugin Name: Safaria Elementor Widget
 * Description: Display elementor widget for Safaria theme.
 * Plugin URI:  https://twitter.com/codeopus
 * Version:     1.2.0
 * Author:      codeopus
 * Author URI:  https://twitter.com/codeopus
 * Text Domain: safaria-elementor-widget
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Main Elementor Abc Team Member Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class Safaria_Elementor_Widget {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'plugins_loaded', [ $this, 'on_plugins_loaded' ] );

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'safaria-elementor-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

	}

	/**
	 * On Plugins Loaded
	 *
	 * Checks if Elementor has loaded, and performs some compatibility checks.
	 * If All checks pass, inits the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_plugins_loaded() {

		if ( $this->is_compatible() ) {
			add_action( 'elementor/init', [ $this, 'init' ] );
		}

	}

	/**
	 * Compatibility Checks
	 *
	 * Checks if the installed version of Elementor meets the plugin's minimum requirement.
	 * Checks if the installed PHP version meets the plugin's minimum requirement.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function is_compatible() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return false;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return false;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return false;
		}

		return true;

	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {
		global $elementor_widget_blacklist;
		$this->i18n();

		// Add Plugin actions
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );
		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		add_action( 'elementor/elements/categories_registered', [ $this, 'add_elementor_widget_categories' ] );

		$elementor_widget_blacklist = ['wp-widget-safariaportfolio_widget', 'wp-widget-safariadestination_widget', 'wp-widget-safariatestimonialslider_widget', 'wp-widget-safariaproductslider_widget'];

		add_action('elementor/widgets/widgets_registered', function($widgets_manager){
			global $elementor_widget_blacklist;
			foreach($elementor_widget_blacklist as $widget_name){
			  $widgets_manager->unregister_widget_type($widget_name);
			}
		  }, 15);
	}
	

	public function add_elementor_widget_categories( $elements_manager ) {

		$elements_manager->add_category(
			'safaria',
			[
				'title' => __( 'Safaria', 'safaria-elementor-widget')
			]
		);
	}
	
	

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// Include Widget files
		require_once( __DIR__ . '/widgets/contact-form-7-widget.php' );
		require_once( __DIR__ . '/widgets/destination-widget.php' );
		require_once( __DIR__ . '/widgets/product-slider-widget.php' );
		require_once( __DIR__ . '/widgets/portfolio-widget.php' );
		require_once( __DIR__ . '/widgets/testimonial-slider-widget.php' );

		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Safaria_CF7_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Safaria_DSTN_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Safaria_PS_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Safaria_PF_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Safaria_TS_Widget() );

	}

	/**
	 * Init Controls
	 *
	 * Include controls files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_controls() {


	}
	
	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'safaria-elementor-widget' ),
			'<strong>' . esc_html__( 'Safaria Elementor Widget', 'safaria-elementor-widget' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'safaria-elementor-widget' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'safaria-elementor-widget' ),
			'<strong>' . esc_html__( 'Safaria Elementor Widget', 'safaria-elementor-widget' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'safaria-elementor-widget' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'safaria-elementor-widget' ),
			'<strong>' . esc_html__( 'Safaria Elementor Widget', 'safaria-elementor-widget' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'safaria-elementor-widget' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

}

Safaria_Elementor_Widget::instance();
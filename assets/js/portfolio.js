function Sfrisotope() {

  "use strict"; 
	
  // for each filters
  jQuery('.option-set').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
	
    // init isotope
    var grid = $buttonGroup.data('target');
    
      imagesLoaded(grid, function(){
        jQuery(grid).isotope({
          itemSelector: '.element',
          animationOptions: {
          duration: 750,
          easing: 'linear',
          queue: false
        }
        });
      });
   
    
    // button click
    $buttonGroup.on( 'click', 'li', function() {
      var $this = jQuery( this );
      // filter isotope
      var filterValue = $this.attr('data-filter');
      jQuery(grid).isotope({ filter: filterValue })
      // change selected
      $buttonGroup.find('.selected').removeClass('selected');
      $this.addClass('selected');
    });
  });

  jQuery(".fancybox").fancybox({
    padding: 0,
    openEffect: "elastic",
    openSpeed: 250,
    closeEffect: "elastic",
    closeSpeed: 250,
    closeClick: false,
    helpers: { title: { type: "outside" }, overlay: { css: { background: "rgba(0,0,0,0.85)" } }, media: {} },
  });


};
Sfrisotope();
var swiper = null;

function SfrProductSliderInitSwiper() {
  var sfrProductSwiper = new Swiper('.sfr-product-swiper', {
    initialSlide: 0,
    slidesPerView: 4,
    spaceBetween:30,
    centeredSlides: true,
    loop: ps.loop,
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
          observer: true,
          observeParents: true,
          slidesPerView:1,
          spaceBetween: 30,
          centeredSlides: false
        },
        480: {
          observer: true,
          observeParents: true,
          slidesPerView: 1,
          spaceBetween: 30,
          centeredSlides: false
        },
        640: {
          observer: true,
          observeParents: true,
          slidesPerView: 2,
          spaceBetween: 30,
          centeredSlides: false
        },
        1024: {
          observer: false,
          observeParents: false,
          slidesPerView: 2,
          spaceBetween: 30,
          centeredSlides: true
        },
        1200: {
          slidesPerView: 2,
          spaceBetween: 30,
          centeredSlides: true
        }
      }
});
}

var timer;

window.addEventListener('resize', function () {
  if (timer) {
    clearTimeout(timer);
  }

  timer = setTimeout(SfrProductSliderInitSwiper, 200);
});

SfrProductSliderInitSwiper();
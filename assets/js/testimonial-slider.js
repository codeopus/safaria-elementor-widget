window.onload = function () { 

    var mySwiper = new Swiper('.testimonial-swiper', {
        slidesPerView: 1,
        spaceBetween:0,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    mySwiper.update();

 }
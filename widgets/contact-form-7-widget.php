<?php
/**
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Safaria_CF7_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'SFRCF7';
	}

	/**
	 * Get widget title.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Safaria - Contact Form 7', 'safaria-elementor-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-form-horizontal';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team member widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'safaria' ];
	}

	/**
	 * Register script & style widget.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function __construct($data = [], $args = null) {
		parent::__construct($data, $args);
		wp_register_style( 'sfr-cf7', plugin_dir_url( __DIR__ ) . 'assets/css/contact-form.css' );
	}


	public function get_script_depends() {
		return [];
	}

	public function get_style_depends() {
		return [ 'sfr-cf7' ];
	}

	public function get_contactform_forms(){
        $formlist = array();
        $forms_args = array( 'posts_per_page' => -1, 'post_type'=> 'wpcf7_contact_form' );
        $forms = get_posts( $forms_args );
        if( $forms ){
            foreach ( $forms as $form ){
                $formlist[$form->ID] = $form->post_title;
            }
        }else{
            $formlist['0'] = __('Form not found','safaria-elementor-widget');
        }
        return $formlist;
    }

	/**
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {


		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Contact Form 7', 'safaria-elementor-widget' )
			]
		);


		$this->add_control(
			'contact_form_id',
			[
				'label' => __( 'Select Form', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => $this->get_contactform_forms(),
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	
	protected function render() {

		$settings   = $this->get_settings_for_display();
        $id         = $this->get_id();

        $this->add_render_attribute( 'sfr_wpform_attr', 'class', 'sfr-contact-form-wrapper' );

        $this->add_render_attribute( 'shortcode', 'id', $settings['contact_form_id'] );
        $shortcode = sprintf( '[contact-form-7 %s]', $this->get_render_attribute_string( 'shortcode' ) );

	?>

	<div <?php echo esc_attr($this->get_render_attribute_string('sfr_wpform_attr')); ?> >
		<?php
			if( !empty( $settings['contact_form_id'] ) ){
				echo do_shortcode( $shortcode ); 
			}else{
				echo '<div class="form_no_select">' .__('Please Select contact form.','safaria-elementor-widget'). '</div>';
			}
		?>
	</div>

	<?php

	}

}
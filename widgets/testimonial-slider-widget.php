<?php
/**
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Safaria_TS_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team-member widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'SFRTestimonialSlider';
	}

	/**
	 * Get widget title.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Safaria - Testimonial Carousel', 'safaria-elementor-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-testimonial';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team member widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'safaria' ];
	}

	/**
	 * Register script & style widget.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function __construct($data = [], $args = null) {
		parent::__construct($data, $args);
		wp_register_script( 'swiper', ELEMENTOR_ASSETS_URL . '/lib/swiper/swiper.js', [ 'jquery' ], false, true );
		wp_register_script( 'sfr-testimonial-slider',  plugin_dir_url( __DIR__ ) . 'assets/js/testimonial-slider.js' , [ 'elementor-frontend', 'jquery', 'swiper' ], '1.0.0', true );
		wp_register_style( 'sfr-testimonial-slider', plugin_dir_url( __DIR__ ) . 'assets/css/testimonial-slider.css' );
	}


	public function get_script_depends() {
		return [ 'swiper', 'sfr-testimonial-slider' ];
	}

	public function get_style_depends() {
		return [ 'sfr-testimonial-slider' ];
	}

	protected function get_testimonial_categories() {
		$categories = get_terms( 
			array(
			'taxonomy' => 'testimonial_category',
			'hide_empty' => false,
			)
		);
		$results = array();
		if ( ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
				$results[ $category->slug ] = $category->name;
			}
		}

		return $results;
	}

	/**
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$argorderby = array ('none' => esc_html('None', 'safaria-elementor-widget'), 'ID' => esc_html('ID', 'safaria-elementor-widget'), 'author' => esc_html('Author', 'safaria-elementor-widget'), 'title' => esc_html('Title', 'safaria-elementor-widget'), 'name' => esc_html('Name', 'safaria-elementor-widget'), 'type' => esc_html('Type', 'safaria-elementor-widget'), 'date' => esc_html('Date', 'safaria-elementor-widget'), 'modified' => esc_html('Modified', 'safaria-elementor-widget'), 'rand' => esc_html('Rand', 'safaria-elementor-widget'), 'menu_order' => esc_html('Menu Order', 'safaria-elementor-widget'));
		$argorder	= array ('asc' => esc_html('ASC', 'safaria-elementor-widget'), 'desc' => esc_html('DESC', 'safaria-elementor-widget'));

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'safaria-elementor-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'skin',
			[
				'label' => __( 'Skin', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'skin-light',
				'options' => [
					'skin-light' => __( 'Light', 'safaria-elementor-widget' ),
					'skin-dark' => __( 'Dark', 'safaria-elementor-widget' ),
				],
			]
		);

		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => $this->get_testimonial_categories(),
			]
		);

		$this->add_control(
			'showpost',
			[
				'label' => __( 'Showpost', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => '3',
			]
		);

		$this->add_control(
			'orderby',
			[
				'label' => __( 'Orderby', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => $argorderby,
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'asc',
				'options' => $argorder,
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	
	protected function render() {
		global $post;
		$settings = $this->get_settings_for_display();

		if( $settings['category'] =="" ) return false;
		

		$args = array(
			'post_type' => 'testimonial',
			'post_status' => 'publish',
			'showposts' => $settings['showpost'],
			'orderby' => $settings['orderby'],
			'order'   => $settings['order'],
			'tax_query' => array(
				array(
					'taxonomy' => 'testimonial_category',
					'terms' => $settings['category'],
					'field' => 'slug',
				)
			)
		);
			
		$looptesti = new WP_Query( $args );
		?>

		<?php if($looptesti->have_posts()){ STATIC $i = 0; $i++; ?>
		

		<div id="testimonial-swiper-<?php echo esc_attr($i);?>" class="testimonial-swiper swiper-container <?php echo $settings['skin'];?>">
			<div class="swiper-wrapper">

				<?php while($looptesti->have_posts()) : $looptesti->the_post(); ?>
				<?php $info = get_post_meta($post->ID, 'cdo_testi_info',true); ?>
				<div class="swiper-slide testimonial-slider-quote">

					<?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) { ?>
						<div class="cdo-testimonial-slider-img2">
							<?php echo get_the_post_thumbnail($looptesti->ID, 'full', array('class' => '')); ?>
						</div>
					<?php } ?>
					<p><?php echo get_the_content($looptesti->ID);?></p>
					<div class="testimonial-slider-info"><strong><?php echo get_the_title($looptesti->ID);?></strong> <?php echo esc_attr($info);?></div>

				</div>
				<?php endwhile; wp_reset_postdata(); ?>

			</div>

			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>

		</div>

		<?php } ?>

		<?php

	}

}
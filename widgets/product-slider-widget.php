<?php
/**
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Safaria_PS_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team-member widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'SFRProductSlider';
	}

	/**
	 * Get widget title.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Safaria - Product Carousel', 'safaria-elementor-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-product-images';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team member widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'safaria' ];
	}

	/**
	 * Register script & style widget.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function __construct($data = [], $args = null) {
		parent::__construct($data, $args); 
		$loop = isset($data["settings"]['loop']) ? $data["settings"]['loop'] : true;
		wp_register_script( 'swiper', ELEMENTOR_ASSETS_URL . '/lib/swiper/swiper.js', [ 'jquery' ], false, true );
		wp_register_script( 'sfr-product-slider',  plugin_dir_url( __DIR__ ) . 'assets/js/product-slider.js' , [ 'elementor-frontend', 'jquery', 'swiper' ], '1.0.0', true );
		wp_register_style( 'sfr-product-slider', plugin_dir_url( __DIR__ ) . 'assets/css/product-slider.css' );
		wp_localize_script('sfr-product-slider', 'ps', 
		array(
			'loop' => ( $loop == 'yes' ? true : false)
		));
		add_action( 'elementor/editor/before_enqueue_scripts', function() {
			wp_enqueue_script(
				'plugin-name-editor',
				plugin_dir_url( __DIR__ ) . 'assets/js/product-slider.js',
				[
					'elementor-editor', // dependency
				],
				'1.0',
				true // in_footer
			);
		 } );
	}


	public function get_script_depends() {
		return [ 'swiper', 'sfr-product-slider' ];
	}

	public function get_style_depends() {
		return [ 'sfr-product-slider' ];
	}

	protected function get_ps_categories() {
		$categories = get_terms( 
			array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			)
		);
		$results = array();
		if ( ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
				$results[ $category->slug ] = $category->name;
			}
		}

		return $results;
	}

	/**
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$argorderby = array ('none' => esc_html('None', 'safaria-elementor-widget'), 'ID' => esc_html('ID', 'safaria-elementor-widget'), 'author' => esc_html('Author', 'safaria-elementor-widget'), 'title' => esc_html('Title', 'safaria-elementor-widget'), 'name' => esc_html('Name', 'safaria-elementor-widget'), 'type' => esc_html('Type', 'safaria-elementor-widget'), 'date' => esc_html('Date', 'safaria-elementor-widget'), 'modified' => esc_html('Modified', 'safaria-elementor-widget'), 'rand' => esc_html('Rand', 'safaria-elementor-widget'), 'menu_order' => esc_html('Menu Order', 'safaria-elementor-widget'));
		$argorder	= array ('asc' => esc_html('ASC', 'safaria-elementor-widget'), 'desc' => esc_html('DESC', 'safaria-elementor-widget'));

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'safaria-elementor-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'type',
			[
				'label' => __( 'Type', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'featured',
				'options' => [
					'featured' => __( 'Featured', 'safaria-elementor-widget' ),
					'onsale' => __( 'On Sale', 'safaria-elementor-widget' ),
					'bestseller' => __( 'Best Seller', 'safaria-elementor-widget' ),
					'popular' => __( 'Best Rating', 'safaria-elementor-widget' ),
					'' => __( 'Latest Product', 'safaria-elementor-widget' ),
				],
			]
		);

		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'default' => '',
				'options' => $this->get_ps_categories(),
			]
		);


		$this->add_control(
			'showpost',
			[
				'label' => __( 'Showpost', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => '3',
			]
		);

		$this->add_control(
			'orderby',
			[
				'label' => __( 'Orderby', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => $argorderby,
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'asc',
				'options' => $argorder,
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'slider_section',
			[
				'label' => __( 'Carousel options', 'safaria-elementor-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'loop',
			[
				'label' => __( 'Loop carousel item', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'pagination',
			[
				'label' => __( 'Pagination', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'navigation',
			[
				'label' => __( 'Navigation', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	
	protected function render() {
		
		global $woocommerce, $wpdb, $yith_wcwl, $product ;

		if ( !class_exists( 'WooCommerce' ) ) return false;

		$settings = $this->get_settings_for_display();

		$query_args = array('posts_per_page' => $settings['showpost'],  'post_status' => 'publish', 'post_type' => 'product' );

		if(!empty($settings['category'])){
			$query_args['tax_query'][] = array(
					'taxonomy'         => 'product_cat',
					'terms'            => $settings['category'],
					'field'            => 'slug',
					'operator'         => 'IN'
			);
		 }

		switch($settings['type']):

			case 'featured':
				$query_args['orderby'] = $settings['orderby'];
				$query_args['order'] = $settings['order'];
				$query_args['tax_query'][] = array(
				        'taxonomy'         => 'product_visibility',
				        'terms'            => 'featured',
				        'field'            => 'name',
				        'operator'         => 'IN',
				        'include_children' => false,
				 );

			break;

			case 'onsale':
				$query_args['orderby'] = $settings['orderby'];
				$query_args['order'] = $settings['order'];
				$query_args['meta_query'][] =   array(
					array(
						'key'           => '_sale_price',
						'value'         => 0,
						'compare'       => '>',
						'type'          => 'numeric'
					)
				);

			break;

			case 'bestseller':
				$query_args['orderby'] = 'meta_value_num';
				$query_args['order'] = $settings['order'];
				$query_args['meta_query'][] =   array(
					array(
						'key'           => 'total_sales',
						'value'         => 0,
						'compare'       => '>',
						'type'          => 'numeric'
					)
				);

			break;

			case 'popular':
				$query_args['orderby'] = 'meta_value_num';
				$query_args['order'] = $settings['order'];
				$query_args['meta_query'][] =   array(
					array(
						'key'           => '_wc_average_rating',
						'value'         => 0,
						'compare'       => '>',
						'type'          => 'numeric'
					)
				);

			break;

			default:
				$query_args['orderby'] = $settings['orderby'];
				$query_args['order'] = $settings['order'];
				$query_args['meta_query'] = array();
				$query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
				$query_args['meta_query']   = array_filter( $query_args['meta_query'] );

			break;

		endswitch;

		$q = new WP_Query( $query_args );
		
		$out ='';

		if($q->have_posts()) : STATIC $i = 0; $i++;
		
				
			$out.='<div id="sfr-product-slider' .  esc_attr($i) . '" class="sfr-product-swiper swiper-container">
					  <div class="swiper-wrapper sfr-product-wrapper">';
					  
					  while($q->have_posts()): $q->the_post();
					  
						$thecolor = get_post_meta( $q->post->ID, 'codeopus_product_slider_style', true );
						$thesubtitle = get_post_meta( $q->post->ID, 'codeopus_product_slider_subtitle', true );
						$thethumb = get_post_meta( $q->post->ID, 'codeopus_product_slider_thumb_id', 1 );
						
						
						$getthecolor =($thecolor!='' ? 'box-'.$thecolor : 'box-purple');
						$getthebuttoncolor =($thecolor!='' ? 'btn-'.$thecolor : 'btn-purple');
						
						$thumbid   = get_post_thumbnail_id();
						$the_attachment_url =  wp_get_attachment_image_url($thethumb, 'product-thumb-small2');
						$the_thumbnail_src = wp_get_attachment_image_src($thumbid, 'product-thumb-small2');
						$getthumb = ($thethumb!='0' && $thethumb!='' ? $the_attachment_url : $the_thumbnail_src[0]);
												
						
						$postexcerpt = get_the_excerpt();  
						$len = 40; 
						$newExcerpt = substr($postexcerpt, 0, $len);
						if( strlen((string)$newExcerpt) < strlen((string)$q->post_excerpt)) { $newExcerpt = $newExcerpt.'...';}
						$getsubtitle = ($thesubtitle!='' ? $thesubtitle : $newExcerpt);
						
					
						$product = new WC_Product( get_the_ID() );
						$price = $product->get_price();
						$price_html = $product->get_price_html();
						$theprice = $price_html;
					  
						$out.='<div class="swiper-slide sfr-product-item">
						  <div class="box-ex-promo '.esc_attr($getthecolor).'">
							<div class="top-section">
							  <div class="ex-one" style="background:url('.esc_url($getthumb).') no-repeat right top">
								<div class="left-ex">
								  <h4>'. $theprice .'</h4>
								  <p class="text-left">
									'.esc_html(get_the_title($q->post->ID)).'
								  </p>
								</div>
							  </div>
							  <div class="bottom-section">
								<div class="bottom-wrap">
								  <div class="bottom-table">
									<div class="bottom-cell">
									  <a class="btn button-default '.esc_attr($getthebuttoncolor).'" href="'.esc_url(get_permalink($q->post->ID)).'">'.esc_html('Read More', 'safaria-elementor-widget').'</a>
									</div>
								  </div>
								</div>';
								if($getsubtitle!=''){
								$out .='<span>'.wp_kses( $getsubtitle, array('span' => array('class')) ).'</span>
								<div class="bar-section">
								  &nbsp;
								</div>';
								}
							  $out .='</div>
							</div>
						  </div>
						</div>';
						
						endwhile; wp_reset_postdata();
						
					  $out.='</div>';
					  
					  if($settings['pagination'] == "yes") $out.='<div class="swiper-pagination"></div>';
					  if($settings['navigation'] == "yes") $out.='<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
					  
					  $out.='</div>';
		endif; 

	echo $out;


	}

}
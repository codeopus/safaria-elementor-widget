<?php
/**
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Safaria_DSTN_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team-member widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'SFRDestinationWidget';
	}

	/**
	 * Get widget title.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Safaria - Destination Widget', 'safaria-elementor-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-meetup';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team member widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'safaria' ];
	}

	/**
	 * Register script & style widget.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function __construct($data = [], $args = null) {
		parent::__construct($data, $args);
		wp_register_style( 'sfr-dstn', plugin_dir_url( __DIR__ ) . 'assets/css/destination.css' );
	}


	public function get_script_depends() {
		return [];
	}

	public function get_style_depends() {
		return [ 'sfr-dstn' ];
	}

	protected function get_dstn_categories() {
		$categories = get_terms( 
			array(
			'taxonomy' => 'destination_category',
			'hide_empty' => false,
			)
		);
		$results = array();
		if ( ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
				$results[ $category->slug ] = $category->name;
			}
		}

		return $results;
	}

	/**
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$argorderby = array ('none' => esc_html('None', 'safaria-elementor-widget'), 'ID' => esc_html('ID', 'safaria-elementor-widget'), 'author' => esc_html('Author', 'safaria-elementor-widget'), 'title' => esc_html('Title', 'safaria-elementor-widget'), 'name' => esc_html('Name', 'safaria-elementor-widget'), 'type' => esc_html('Type', 'safaria-elementor-widget'), 'date' => esc_html('Date', 'safaria-elementor-widget'), 'modified' => esc_html('Modified', 'safaria-elementor-widget'), 'rand' => esc_html('Rand', 'safaria-elementor-widget'), 'menu_order' => esc_html('Menu Order', 'safaria-elementor-widget'));
		$argorder	= array ('asc' => esc_html('ASC', 'safaria-elementor-widget'), 'desc' => esc_html('DESC', 'safaria-elementor-widget'));

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'safaria-elementor-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);


		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'default' => '',
				'options' => $this->get_dstn_categories(),
			]
		);

		$this->add_control(
			'column',
			[
				'label' => __( 'Column', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '3',
				'options' => [
					'2' => '2',
					'3' => '3',
					'4' => '4'
				],
			]
		);

		$this->add_control(
			'showpost',
			[
				'label' => __( 'Showpost', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => '3',
			]
		);

		$this->add_control(
			'showtitle',
			[
				'label' => __( 'Show Title?', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'orderby',
			[
				'label' => __( 'Orderby', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => $argorderby,
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'asc',
				'options' => $argorder,
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	
	protected function render() {
		global $post, $paged;
		$settings = $this->get_settings_for_display();
		if( $settings['category'] =="" ) return false;
	
		$getcolumn = ( $settings['column'] =="3" ? 'column3' : ( $settings['column'] =="4" ? 'column4' : 'column2'));
		
		$query_args = array(
		'post_status' => 'publish',
		'post_type' => 'destination',
		'posts_per_page' => $settings['showpost'],
		'paged' => get_query_var( 'paged' ),
		'orderby' => $settings['orderby'],
		'order'=> $settings['order'],
		'tax_query' => array(
			array(
				'taxonomy' => 'destination_category',
				'terms' => $settings['category'],
				'field' => 'slug',
			)
		)
		);
		
		$loopdestination = new WP_Query( $query_args );
		
		$out ='';
		
		if($loopdestination->have_posts()):
		
		$i = 1;
		$out .='<ul class="destination-list '.esc_attr($getcolumn).'">';
		while($loopdestination->have_posts()) : $loopdestination->the_post();
				
		
		$out .='<li>';
		$out .='<a href="'.esc_url(get_permalink($loopdestination->ID)).'" title="'.esc_attr(get_the_title($loopdestination->ID)).'">';
		$out .='<div class="div-img">';
		if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {
		$out.= get_the_post_thumbnail($loopdestination->ID, 'large', array('class'=>''));
		}
		
		$out .='<div class="round-des">';
		$out .= $i;
		$out .='</div>';
		
		$out .='<div class="wrap-text-destination">';
		
			if( $settings['showtitle'] =="yes"){
			$out .='<div class="destination-title">';
			$out .= get_the_title($loopdestination->ID);
			$out .='</div>';
			}
		
		$out .='</div>';
		
		$out .='</div>';
		$out .='</a>';
		$out .='</li>';
		
		$i++;
		
		endwhile; wp_reset_postdata();
		$out .='</ul>';
		
		endif;

		echo $out;

	}

}
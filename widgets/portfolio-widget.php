<?php
/**
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class Safaria_PF_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve team-member widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'SFRPortfolioWidget';
	}

	/**
	 * Get widget title.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Safaria - Portfolio Widget', 'safaria-elementor-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the team member widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'safaria' ];
	}

	/**
	 * Register script & style widget.
	 *
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function __construct($data = [], $args = null) {
		parent::__construct($data, $args);
		wp_register_script( 'isotope',  plugin_dir_url( __DIR__ ) . 'assets/js/isotope.js' , [ 'elementor-frontend', 'jquery', ], '', true );
		wp_register_script( 'imagesloaded',  plugin_dir_url( __DIR__ ) . 'assets/js/imagesloaded.js' , [ 'elementor-frontend', 'jquery', ], '', true );
		wp_register_script( 'fancybox',  plugin_dir_url( __DIR__ ) . 'assets/js/fancybox.js' , [ 'elementor-frontend', 'jquery', ], '', true );
		wp_register_script( 'fancybox-media',  plugin_dir_url( __DIR__ ) . 'assets/js/fancybox-media.js' , [ 'elementor-frontend', 'jquery', ], '', true );
		wp_register_script( 'sfr-pf',  plugin_dir_url( __DIR__ ) . 'assets/js/portfolio.js' , [ 'elementor-frontend', 'jquery', 'isotope', 'imagesloaded'], '1.0.0', true );
		wp_register_style( 'sfr-pf', plugin_dir_url( __DIR__ ) . 'assets/css/portfolio.css' );
		wp_register_style( 'fancybox', plugin_dir_url( __DIR__ ) . 'assets/css/fancybox.css' );
	}


	public function get_script_depends() {
		return ['isotope', 'imagesloaded', 'fancybox', 'fancybox-media', 'sfr-pf'];
	}

	public function get_style_depends() {
		return [ 'sfr-pf', 'fancybox' ];
	}

	protected function get_pf_categories() {
		$categories = get_terms( 
			array(
			'taxonomy' => 'portfolio_category',
			'hide_empty' => false,
			)
		);
		$results = array();
		if ( ! is_wp_error( $categories ) ) {
			foreach ( $categories as $category ) {
				$results[ $category->slug ] = $category->name;
			}
		}

		return $results;
	}

	/**
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$argorderby = array ('none' => esc_html('None', 'safaria-elementor-widget'), 'ID' => esc_html('ID', 'safaria-elementor-widget'), 'author' => esc_html('Author', 'safaria-elementor-widget'), 'title' => esc_html('Title', 'safaria-elementor-widget'), 'name' => esc_html('Name', 'safaria-elementor-widget'), 'type' => esc_html('Type', 'safaria-elementor-widget'), 'date' => esc_html('Date', 'safaria-elementor-widget'), 'modified' => esc_html('Modified', 'safaria-elementor-widget'), 'rand' => esc_html('Rand', 'safaria-elementor-widget'), 'menu_order' => esc_html('Menu Order', 'safaria-elementor-widget'));
		$argorder	= array ('asc' => esc_html('ASC', 'safaria-elementor-widget'), 'desc' => esc_html('DESC', 'safaria-elementor-widget'));

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'safaria-elementor-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'filter',
			[
				'label' => __( 'Filter', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'imagesize',
			[
				'label' => __( 'Image Size', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'large',
				'options' => [
					'thumbnail' => __( 'Thumbnail', 'safaria-elementor-widget' ),
					'medium' => __( 'Medium', 'safaria-elementor-widget' ),
					'large' => __( 'Large', 'safaria-elementor-widget' ),
				],
			]
		);


		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'default' => '',
				'options' => $this->get_pf_categories(),
			]
		);

		$this->add_control(
			'column',
			[
				'label' => __( 'Column', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '3',
				'options' => [
					'2' => '2',
					'3' => '3',
					'4' => '4'
				],
			]
		);

		$this->add_control(
			'showpost',
			[
				'label' => __( 'Showpost', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => '3',
			]
		);

		$this->add_control(
			'zoomicon',
			[
				'label' => __( 'Zoom icon', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'linkicon',
			[
				'label' => __( 'Link icon', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);


		$this->add_control(
			'showtitle',
			[
				'label' => __( 'Show Title', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);

		$this->add_control(
			'showdesc',
			[
				'label' => __( 'Show Description', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => __( 'Yes', 'safaria-elementor-widget' ),
					'no' => __( 'No', 'safaria-elementor-widget' )
				],
			]
		);


		$this->add_control(
			'orderby',
			[
				'label' => __( 'Orderby', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => $argorderby,
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'safaria-elementor-widget' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => $argorder,
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	
	protected function render() {
		global $post, $paged;
		$settings = $this->get_settings_for_display();
		if( $settings['category'] =="" ) return false;
	
		$column = ($settings['column'] =="3" ? "col-3" : ($settings['column'] =="4" ? "col-4" : ($settings['column'] =="1" ? "col-1" : "col-2")));
		
		$imagesize = ($settings['imagesize'] !='' ? $settings['imagesize'] : 'large'); $query_args['orderby'] = $settings['orderby'];
		
		$query_args = array(
			'post_type' => 'portfolio',
			'posts_per_page' => $settings['showpost'],
			'paged' => get_query_var( 'paged' ),
			'orderby' => $settings['orderby'],
			'order'=> $settings['order'],
			'tax_query' => array(
				array(
					'taxonomy' => 'portfolio_category',
					'terms' => $settings['category'],
					'field' => 'slug',
				)
		));

		if(is_singular('portfolio')) $query_args['post__not_in'] = array( $post->ID);

		$q = new WP_Query( $query_args );
		
		$out='';
		
		if($q->have_posts()) :

		STATIC $x = 0;
		$x++;
		
		if($settings['filter']=="yes"){
			$out.='<div id="sfr-portfolio-filternav">';
				$out.='<ul class="option-set" data-target=".thegrid-'.esc_attr($x).'">';
					
					$out.='<li><span><i class="fa-bars"></i></span></li>';
					$out.='<li class="selected" data-filter="*"><span>'.__('All','safaria-elementor-widget').'</span></li>';
					
					if($settings['category']){
						foreach ($settings['category'] as $pfcat) {
						$cat = get_term_by('slug', $pfcat, 'portfolio_category');
						$out.='<li data-filter=".'.esc_attr($cat->slug).'"><span>'.esc_attr($cat->name).'</span></li>';
						}
					}else{
						$pf_categories = get_categories('taxonomy=portfolio_category&orderby=ID&title_li=&hide_empty=0');
						foreach ($pf_categories as $cat) {
							$out .='<li data-filter=".'.esc_attr($cat->slug).'"><span>'.esc_attr($cat->name).'</span></li>';
						}
						
					}
					
				$out.='</ul>';
			$out.='</div>';                                             
		}
		
		
		$out.='<div id="sfr-portfolio-'.esc_attr($x).'" class="sfr-portfolio-container thegrid-'.esc_attr($x).'">';
		
		while($q->have_posts()): $q->the_post();
		
		$thumb   = get_post_thumbnail_id();
		$portfolioimg = wp_get_attachment_url($thumb,'full');
		$lightbox = get_post_meta($q->post->ID, 'cdo_lightbox_url',true);
		$customlink = get_post_meta($q->post->ID, 'cdo_custom_link',true);
		$zoom = ($lightbox!="" ? $lightbox : $portfolioimg);
		$link = ($customlink!="" ?  $customlink: get_permalink());
		
		
			$out .='<div class="'.esc_attr($column).' element ';
					$cats = get_the_terms($q->post->ID,'portfolio_category');
					foreach ($cats as $cat ) {
						$out .= $cat->slug." ";
					}      
					$out .='">';
				$out.='<figure>';
					if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {
					$out.= get_the_post_thumbnail($q->post->ID, $imagesize, array('class'=>''));
					}
					
					if($settings['linkicon']=='yes' || $settings['zoomicon']=='yes'){
					$out.='<div class="portfolio-button-container">';
					$out.='<div class="portfolio-button">';
					
						$addclass= ($settings['linkicon']=="yes" ? "with-permalink" : ''); 
						$addclass2= ($settings['zoomicon']!="yes" ? "no-zoom" : ''); 
						
						$out.='<div class="portfolio-button-icon '.esc_attr($addclass).' '.esc_attr($addclass2).'">';
						if($settings['zoomicon']=="yes"){$out.='<a class="fancybox" href="'.esc_url($zoom).'" data-fancybox-group="'.esc_attr('gallery').'" title="'.esc_attr(get_the_title()).'"><i class="fa-search"></i></a>';}
						if($settings['linkicon']=="yes"){$out.='<a class="permalink" href="'.esc_url($link).'" title="'.sprintf( esc_attr__( 'Permalink to %s', 'safaria-elementor-widget' ), the_title_attribute( 'echo=0' ) ).'"><i class="fa-link"></i></a>';}
						$out.='</div>';
					$out.='</div>';
					$out.='</div>';
					}
					
				$out.='</figure>';
				if($settings['showtitle']=="yes"){$out.='<h3 class="portfolio-title">'.get_the_title($q->post->ID).'</h3>';}
				if($settings['showdesc']=="yes" && $q->post->post_excerpt!=""){$out .= '<p class="portfolio-excerpt">'.get_the_excerpt($q->post->ID).'</p>';}			
			$out.='</div>';
			
		endwhile; wp_reset_postdata();
		
		$out.='</div>';
		
		endif;
		
		echo $out;

	}

}